export interface BrokerDto {
  id: number;
  name: string;
  contact: string;
  logo: string;
  country: string;
}
