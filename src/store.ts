import axios from "axios";
import { createStore } from "vuex";

import { BrokerDto } from "./interfaces/BrokerDto";

// Create a new store instance.
const store = createStore({
  state() {
    return {
      brokersList: [] as BrokerDto[],
    };
  },
  mutations: {
    setBrokersList(state, brokersList) {
      state.brokersList = brokersList;
    },
  },
  actions: {
    getBrokersList({ commit }) {
      axios.get("http://localhost:5000/brokers").then((response) => {
        commit("setBrokersList", response.data);
      });
    },
  },
});

export default store;
