# default

## Project setup

```
# npm
npm install


```

### Compiles and hot-reloads for development

```
# npm
npm run dev

```

### Compile JSON server

```
# json-server
json-server brokers.json --port 5000

```

### Compiles and minifies for production

```
# npm
npm run build
```

### Customize configuration

See [Configuration Reference](https://vitejs.dev/config/).
